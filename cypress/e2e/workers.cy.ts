describe('workers', () => {

  const uniqueName = "Joe" + Date.now();

  before(() => {
    cy.visit('/auth/login');
    cy.fixture('auth').then((auth) =>
      cy.login(auth.username, auth.password).then((btnLogin) => cy.wrap(btnLogin).should('not.exist')));
    cy.visit('pages/workers');
  })

  it('should add worker', () => {
    cy.contains('add worker').click();
    cy.get('#add-worker-form input[name=name]').type(uniqueName);
    cy.get('#add-worker-form input[name=phone]').type("1234");
    //cy.get('#add-worker-form select').select('support');
    cy.get('#add-worker-form [name=category]').click().get('mat-option').contains('support').click();

    cy.get('#add-worker-form').find('button').click();
    cy.get('#add-worker-form').should('not.exist');

    /* search added */
    cy.get('#search-by-name').type(uniqueName);
    cy.get('tbody').find('tr').should('have.length', 1);
  })
})
