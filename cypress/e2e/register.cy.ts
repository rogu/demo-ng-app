describe('register view', () => {
  let username: string;

  beforeEach(() => {
    cy.visit('/auth/register');
    cy.fixture('auth').then((auth: any) => (username = auth.username));
  });

  context('register user should show error', () => {
    it('when user exists', () => {
      cy.intercept('https://auth.debugger.pl/exists?username=' + username).as('resp');

      if (cy.get('mat-snack-bar-container')) {
        cy.get('mat-snack-bar-container').find('button').click();
      }

      cy.get('#send-form-btn').click();

      cy.get('#register-form input[name=username]')
        .type(username)
        .closest('mat-form-field')
        .find('mat-error')
        .should('contain.text', 'użytkownik już istnieje');

      /* cy.wait('@resp').then((obj: any) => {
        expect(obj.response.body.error).equal('username exists');
      }) */
    });

    it('when passwords are not equals', () => {
      cy.get('#register-form input[name=password]').type('Admin1');

      cy.get('#register-form input[name=confirmPassword]').type('Admin2', { force: true });

      if (cy.get('mat-snack-bar-container')) {
        cy.get('mat-snack-bar-container').find('button').click();
      }

      cy.get('#send-form-btn').click();

      cy.get('#register-form input[name=confirmPassword]')
        .closest('mat-form-field')
        .find('mat-error')
        .should('contain.text', 'wyrażenia nie są zgodne');
    });
  });
});
