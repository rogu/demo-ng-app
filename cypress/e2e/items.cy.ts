import { ItemModel } from "src/app/pages/items/services/items.models";

describe('items view', () => {

  let itemMock: ItemModel;

  before(() => {
    cy.visit('login');
    cy.fixture('item').then((item) => itemMock = { ...item.data, title: item.data.title + Date.now() });
    cy.fixture('auth').then((auth) =>
      cy.login(auth.username, auth.password).then((btnLogin) => cy.wrap(btnLogin).should('not.exist')));
  })

  beforeEach(() => {
    cy.restoreLocalStorage();
    cy.visit('pages/items');
  });

  afterEach(() => {
    cy.saveLocalStorage();
  });

  it('should add & remove item', () => {

    /* add item */
    cy.contains('add item').click();
    //cy.intercept('https://api.debugger.pl/utils/exists?title=' + itemMock.title).as('respTitle');
    cy.intercept('https://api.debugger.pl/utils/upload').as('respFile');
    cy.fillItemForm(itemMock);
    cy.wait(['@respFile']).then(({ response: { body: { imgSrc } } }) => {
      expect(imgSrc).exist;
      cy.get('#add-item-form img').should('exist');
      cy.get('#add-item-form').find('button').click();
      cy.get('#add-item-form').should('not.exist');
    })

    /* search & remove added item */
    cy.get('#search-by-title').type(itemMock.title);
    cy.get('app-datagrid td').contains(itemMock.title).should('exist');
    cy.get('app-datagrid tbody').find('tr').should('have.length', 1)
    cy.get('app-datagrid td').contains('remove').click();
    cy.get('app-datagrid').contains(itemMock.title).should('not.exist');
  })

  context('should show', () => {
    it('3 errors when entire form is empty', () => {
      cy.contains('add item').click();
      cy.get('#add-item-form').find('button').click();
      cy.get('mat-error').should('have.length', 3);
    })

    it('1 errors when title field is empty', () => {
      cy.contains('add item').click();
      cy.fillItemForm({ ...itemMock, title: null });
      cy.get('#add-item-form').find('button').click();
      cy.get('mat-error').should('have.length', 1);
    })
  })

})
