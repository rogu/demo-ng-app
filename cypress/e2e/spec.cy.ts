describe('Demo App', () => {
  beforeEach(() => {
    cy.restoreLocalStorage();
  });

  afterEach(() => {
    cy.saveLocalStorage();
  });

  it('Visits the initial project page', () => {
    cy.visit('/');
    cy.contains('Demo Angular App');
  });

  it('should display proper version', () => {
    cy.visit('/');
    cy.task('readFileMaybe', 'package.json').then((textOrNull: any) => {
      const pkg = JSON.parse(textOrNull);
      cy.get('mat-sidenav-content mat-toolbar').then((el: any) => {
        expect(el.text().trim()).to.include(`${pkg.version}`);
      });
    });
  });
});
