describe('Auth', () => {

  before(() => {
    cy.intercept('https://auth.debugger.pl/is-logged').as('logged');
    cy.visit('/');
    cy.wait('@logged').then(({ response: { body: { warning } } }) => {
      expect(warning).to.include('No auth token')
    })
  })

  beforeEach(() => {
    cy.visit('/auth/login');
  })

  it('should display login button after login failure', () => {
    cy.fixture('auth').then((auth) =>
      cy.login(auth.wrongUsername, auth.password).then((btnLogin) =>
        cy.wrap(btnLogin).should('exist')));
  })

  it('should display logout button after login success', () => {
    cy.fixture('auth').then((auth) =>
      cy.login(auth.username, auth.password).then((btnLogin) =>
        cy.wrap(btnLogin).should('not.exist')));
  })

})
