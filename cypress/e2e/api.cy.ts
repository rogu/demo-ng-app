import { Api } from '../../src/app/shared/utils/api';

describe('Api items testing', () => {
  it('should return data', () => {
    cy.request(Api.DATA_ITEMS).then(({ status, body: { data, total } }) => {
      expect(data).exist;
      expect(total).exist;
      expect(status).equal(200);
    })
  })

  it('should return filtered data', () => {
    cy.request(`${Api.DATA_ITEMS}?itemsPerPage=2`).then(({ status, body: { data, total } }) => {
      expect(data.length).equal(2);
    })
  })

  it('should login & create new item', () => {
    cy.request('post', Api.AUTH_LOGIN, { username: 'admin@localhost', password: 'Admin1' }).then(({ body: { data: { accessToken } } }) => {
      cy.request({
        method: 'post',
        url: Api.DATA_ITEMS,
        body: { title: 'xyz' + Date.now(), price: 1, category: 'food' },
        headers: { authorization: accessToken }
      }).then(({ body: { success } }) => expect(success).equal('added'));
    })
  })
})
