declare global {
    namespace Cypress {
        interface Chainable {
            login(username: string, password: string): Cypress.Chainable,
            saveLocalStorage(): Cypress.Chainable,
            restoreLocalStorage(): Cypress.Chainable,
            fillItemForm(data: any): Cypress.Chainable
        }
    }
}

import 'cypress-file-upload';
import './core.commands';
import './items.commands';
