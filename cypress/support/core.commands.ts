
/**
 * Save localstorage
 */

let LOCAL_STORAGE_MEMORY: { [key: string]: string } = {};

Cypress.Commands.add("saveLocalStorage", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
  return cy;
});

Cypress.Commands.add("restoreLocalStorage", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
  return cy;
});

/**
 * Auth
 */

Cypress.Commands.add('login', (username, password) => {

  return cy.fixture('auth').then((auth) => {
    cy.visit('/auth/login');
    cy.intercept('https://auth.debugger.pl/login').as('loginResp');
    cy.get('#login-form')
      .find('input[name="username"]').clear().type(username)
      .closest('#login-form')
      .find('input[name="password"]').clear().type(password);
    cy.get('#login-form button').click();

    if (auth.wrongUsername === username) {
      cy.wait('@loginResp').then(({ response: { body: { error } } }: any) =>
        expect(error).to.include("No user with that email"));
    }
    cy.get('app-auth').contains('log in');
  });

})
