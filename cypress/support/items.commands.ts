Cypress.Commands.add('fillItemForm', ({ title, price, category }: any) => {
  title && cy.get('#add-item-form input[name=title]').type(title);
  cy.get('#add-item-form input[name=price]').type(price);
  cy.get('#add-item-form [name=category]').click().get('mat-option').contains(category).click();
  cy.get('#add-item-form file-upload input[type=file]').attachFile('in.png');
  return cy;
})

