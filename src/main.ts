import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { enableProdMode } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouter } from '@angular/router';
import { routes } from './app/core/app.routes';
import { AppComponent } from './app/core/components/app.component';
import { authInterceptor } from './app/core/interceptors/auth.interceptor';
import { coreInterceptor } from './app/core/interceptors/core.interceptor';
import { notificationInterceptor } from './app/core/interceptors/notification.interceptor';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    provideHttpClient(withInterceptors([coreInterceptor, authInterceptor, notificationInterceptor])),
    provideAnimations(),
    provideRouter(routes),
  ],
}).catch((err) => console.error(err));
