import { AfterViewInit, Component, DestroyRef, inject, input, output, viewChild } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormsModule, NgForm } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { Subscription } from 'rxjs';
import { debounceTime, filter, tap } from 'rxjs/operators';
import { CamelCaseToSignPipe } from '../../pipes/camel-case-to-sign/camel-case-to-sign';
import { SearchControlModel } from './search.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
    `
      mat-slider {
        display: block;
      }
    `,
  ],
  standalone: true,
  imports: [FormsModule, MatFormFieldModule, MatInputModule, MatSliderModule, MatSelectModule, MatOptionModule, MatButtonModule, CamelCaseToSignPipe],
})
export class SearchComponent implements AfterViewInit {
  controls = input<SearchControlModel<any>[]>();
  searchChange = output<any>();
  searchForm = viewChild<NgForm>('searchForm');
  formInitValue!: {};
  formSubscription?: Subscription;
  destroyRef = inject(DestroyRef);

  ngAfterViewInit(): void {
    this.formSubscription = this.searchForm()!
      .valueChanges?.pipe(
        takeUntilDestroyed(this.destroyRef),
        tap((value) => this.controlsReady(value) && (this.formInitValue = value)),
        filter(() => !this.searchForm()!.pristine),
        debounceTime(500)
      )
      .subscribe((value) => this.searchChange.emit(value));
  }
  controlsReady(value: any) {
    return !this.formInitValue && Object.keys(value).length === this.controls()!.length;
  }

  clear() {
    this.searchForm()!.setValue(this.formInitValue);
  }
}
