/* import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { SearchComponent } from './search.component';
import { FieldTypes, SearchControlModel } from './search.model';
import { CamelCaseToSignPipe } from '../../pipes/camel-case-to-sign/camel-case-to-sign';
import { FormsModule, NgForm } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('search component', () => {

  let fixture: ComponentFixture<SearchComponent>,
    instance: SearchComponent,
    element: DebugElement,
    form: NgForm,
    searchConfig: SearchControlModel<any>[];

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [
        FormsModule,
        BrowserAnimationsModule,
        SearchComponent,
        CamelCaseToSignPipe
    ]
});

    fixture = TestBed.createComponent(SearchComponent);
    instance = fixture.componentInstance;
    searchConfig = [
      { name: 'title', tag: FieldTypes.INPUT_TEXT },
      { name: 'price', tag: FieldTypes.INPUT_NUMBER }
    ]
    instance.controls = searchConfig;
    element = fixture.debugElement;
    form = instance.searchForm;
    fixture.detectChanges();
  });

  function updateTitle(value: string) {
    const inputTitle: HTMLInputElement = element.query(By.css('input[id=search-by-title]')).nativeElement;
    inputTitle.value = value;
    inputTitle.dispatchEvent(new Event('input'));
  }

  it('should render inputs according to controls', () => {
    const inputs = element.queryAll(By.css('input'));
    expect(inputs.length).toBe(searchConfig.length);
  });

  it('should update form value after input title change', async () => {
    await fixture.whenStable()
    updateTitle('tomato');
    expect(form.value.title).toBe('tomato');
  });

  it('output should be called when input title changes', () => {
    fixture.whenStable().then(fakeAsync(() => {
      spyOn(instance.searchChange, 'emit');
      expect(instance.searchChange.emit).not.toHaveBeenCalled();
      updateTitle('tomato');
      tick(1000);
      expect(instance.searchChange.emit).toHaveBeenCalled();
    }))
  });

  it('output value should be tomato when input title changes', () => {
    let expectedTitle: any;
    instance.searchChange.subscribe(({ title }: any) => expectedTitle = title);
    fixture.whenStable().then(fakeAsync(() => {
      updateTitle('tomato')
      tick(500)
      expect(expectedTitle).toBe('tomato')
    }))
  });

  it('should clear form.value when clear function is called', async () => {
    await fixture.whenStable()
    updateTitle('tomato');
    expect(form.value).toEqual({ title: 'tomato', price: '' });
    instance.clear();
    expect(form.value).toEqual({ title: '', price: '' });
  });

});
 */
