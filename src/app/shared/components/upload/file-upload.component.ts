import { Component, inject, input, output } from '@angular/core';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'file-upload',
  template: `
        <div>
            <p>File Upload - select file (max 2MB):</p>
            <input type='file' (change)='changeHandler($event)'>
            <span [hidden]='!progress'>{{progress}} %</span>
        </div>`,
  standalone: true
})

export class FileUploadComponent {
  notification = inject(NotificationService);

  progress!: number;
  maxSize: number = 2000000;

  url = input<any>();
  uploaded = output<any>();

  changeHandler(evt: any): void {
    const data: FormData = new FormData();
    const files = evt.target.files;

    if (files.length === 0) {
      return;
    }

    const file: File = files[0];
    if (file.size > this.maxSize) {
      this.notification.showWarning('file is to big; max 2mb');
      return;
    }
    data.append('file', file, file.name);

    const xhr = new XMLHttpRequest();

    xhr.upload.addEventListener('progress', (progressEvent: ProgressEvent) => {
      this.progress = +((progressEvent.loaded / progressEvent.total) * 100).toFixed();
    });

    xhr.addEventListener('load', () => {
      const result = JSON.parse(xhr.response);
      this.uploaded.emit(result.imgSrc);
    });

    xhr.open('POST', this.url(), true);
    xhr.send(data);
  }
}
