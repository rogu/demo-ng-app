import { Component, input, output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Subject } from "rxjs";
import { DataGridRowConfig } from '../data-grid.models';

@Component({
  selector: '[data-grid-row]',
  templateUrl: './data-grid-row.component.html',
  styles: [`
    td {
      padding: 10px !important;
    }
  `],
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, FormsModule, MatButtonModule]
})
export class DataGridRowComponent {
  data = input.required<{ [s: string]: string }>();
  config = input.required<DataGridRowConfig<any>[]>();
  itemAction = input<Subject<any>>();
  onChange = output<any>();
}
