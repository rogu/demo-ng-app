import { TitleCasePipe } from '@angular/common';
import { Component, input, output } from '@angular/core';
import { Subject } from "rxjs";
import { DataGridRowComponent } from './data-grid-row/data-grid-row.component';
import { DataGridRowConfig } from './data-grid.models';

@Component({
  selector: 'app-datagrid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['data-grid.component.scss'],
  standalone: true,
  imports: [DataGridRowComponent, TitleCasePipe]
})

export class DataGridComponent {
  data = input.required<any[]>();
  config = input.required<DataGridRowConfig<any>[]>();
  itemAction = input<Subject<any>>();
  onAction = output<any>();
}
