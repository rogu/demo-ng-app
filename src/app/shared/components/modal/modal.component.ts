import {
  Component,
  Signal,
  TemplateRef,
  contentChild,
  inject,
  input
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  exportAs: 'modal',
  standalone: true,
  imports: [MatButtonModule],
})
export class ModalComponent {
  dialog = inject(MatDialog);
  name = input<string>();
  disabled = input<boolean>();
  tpl: Signal<any> = contentChild(TemplateRef);

  openDialog(): void {
    const dialogRef: MatDialogRef<any> = this.dialog.open(this.tpl(), {
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }
}
