import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpServiceModel, ResponseData } from './http.models';

export abstract class HttpArrayStore<T> implements HttpServiceModel<T> {
  http: HttpClient;
  url: string;
  private _data$ = new BehaviorSubject<T[] | null>(null);
  public get data$(): Observable<T[] | null> {
    return this._data$.asObservable();
  }
  private _total$ = new BehaviorSubject<number | null>(null);
  public get total$(): Observable<number | null> {
    return this._total$.asObservable();
  }

  constructor(http: HttpClient, url: string) {
    this.http = http;
    this.url = url;
  }

  get(params: any): Observable<T[]>;
  get(id: string): Observable<T>;
  get(): Observable<T[]>;
  get(params?: any, id?: string): Observable<any> {
    const url = [this.url, id].filter((a) => a).join('/');
    return this.http.get<ResponseData<T>>(url, { params }).pipe(
      map(({ data, total }: ResponseData<T>) => {
        this._data$.next(data);
        this._total$.next(total);
      })
    );
  }

  add(item: T): Observable<ResponseData<T>> {
    return this.http.post<ResponseData<T>>(this.url, item);
  }

  update(item: T & { id: string }): Observable<ResponseData<T>> {
    return this.http.patch<ResponseData<T>>(`${this.url}/${item.id}`, item);
  }

  remove(id: string): Observable<ResponseData<T>> {
    return this.http.delete<ResponseData<T>>(this.url + '/' + id);
  }
}
