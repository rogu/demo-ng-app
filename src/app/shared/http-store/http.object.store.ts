import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ResponseData } from './http.models';

export abstract class HttpObjectStore<T> {
  http: HttpClient;
  url: string;
  private _data$ = new BehaviorSubject<T | null>(null);
  public get data$(): Observable<T | null> {
    return this._data$.asObservable();
  }

  constructor(http: HttpClient, url: string) {
    this.http = http;
    this.url = url;
  }

  fetch(url?: string): Observable<ResponseData<T>> {
    return this.http.get<ResponseData<T>>(url || this.url).pipe(
      tap(({ data }) => {
        this._data$.next(data);
      })
    );
  }

  post(item: any, url?: string): Observable<ResponseData<T>> {
    return this.http.post<ResponseData<T>>(url || this.url, item);
  }

  update(item: T & { id?: string }): Observable<ResponseData<T>> {
    return this.http.patch<ResponseData<T>>(`${this.url}/${item.id}`, item);
  }

  remove(id: string): Observable<ResponseData<T>> {
    return this.http.delete<ResponseData<T>>(this.url + '/' + id);
  }

  get(id: string): Observable<T> {
    return this.http.get<ResponseData<T>>(`${this.url}/${id}`).pipe(map((resp: ResponseData<T>) => resp.data));
  }
}
