import { Observable } from 'rxjs';

export type HttpMetodNames = 'add' | 'remove' | 'update' | 'more';

export enum Actions {
  Add = 'add',
  Remove = 'remove',
  Update = 'update',
  More = 'more',
}

export interface HttpServiceModel<T> {
  get(params: { [key: string]: any }): Observable<T[]>;
  get(id: string): Observable<T>;
  add(item: any): Observable<ResponseData<T>>;
  update(item: any): Observable<ResponseData<T>>;
  remove(id: string): Observable<ResponseData<T>>;
}

export type ResponseData<T> = {
  data: T[] & T;
  total: number;
  success?: string;
  info?: string;
  warning?: string;
  error?: string;
};
