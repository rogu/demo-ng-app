import { Pipe, PipeTransform } from '@angular/core';

/**
 * This pipe lets you replace capital letter (camelCase) to any sign.
 */
@Pipe({
  name: 'camelCaseToSign',
  standalone: true
})
export class CamelCaseToSignPipe implements PipeTransform {
  transform(expression: string, sign: string) {
    return expression.replace(/[A-Z]/g, (val) => sign + val.toLowerCase());
  }
}
