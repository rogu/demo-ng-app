import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Api } from '../utils/api';

@Injectable({ providedIn: 'root' })
export class CustomServerValidatorsService {
  http = inject(HttpClient);

  checkUsername(params: any) {
    return timer(500).pipe(
      switchMap((_) =>
        this.http
          .get(Api.AUTH_CHECK_USERNAME, { params })
          .pipe(map((resp: any) => (resp.warning ? { [resp.warning]: true } : null)))
      )
    );
  }
}
