import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export class CustomValidators {
  static passwordRegEx(control: AbstractControl): ValidationErrors | null {
    if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$/.test(control.value)) {
      return { passwordRegEx: true };
    }
    return null;
  }

  static equalRequired(control: AbstractControl): ValidationErrors | null {
    if (control.parent) {
      const passwords = [control.parent.value.password, control.value];
      if (passwords[0] !== passwords[1]) {
        return { equalRequired: true };
      }
    }
    return null;
  };

  static atLeastOneShouldBeSelected(group: any): ValidationErrors | null {
    if (Object.values(group.value).some(value => !!value)) {
      return null;
    }
    return { atLeastOne: true };
  };

  static dateShouldBeLessThan(date: string): ValidatorFn {
    return (control: AbstractControl) => {
      if (control.value) {
        const userDate = new Date(control.value).toLocaleDateString();
        return Date.parse(userDate) < Date.parse(date) ? null : { dateShouldBeLessThan: date };
      }
      return { dateShouldBeLessThan: date };
    }
  }
}
