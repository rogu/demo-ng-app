import { Directive, ElementRef, Input, Renderer2, inject } from '@angular/core';

@Directive({
  selector: '[appSetColor]',
  standalone: true
})
export class SetColorDirective {
  renderer = inject(Renderer2);
  element = inject(ElementRef);

  @Input() set appSetColor(value: any) {
    this.renderer.setStyle(this.element.nativeElement, 'background-color', value);
  };
}
