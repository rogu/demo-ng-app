import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';
import { Api } from '../../../shared/utils/api';
import { ItemsDataService } from './items-data.service';

describe('itemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        ItemsDataService,
        AuthService,
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    });
  });

  it('get method should return response', () => {
    const filters = new BehaviorSubject({ title: 'tomato' });
    const expectedReqUrl = Api.DATA_ITEMS + '?title=tomato';
    const itemsService = TestBed.inject(ItemsDataService);
    const http = TestBed.inject(HttpTestingController);
    
    // fake response
    const expectedData = [{ title: 'potatoes' }];
    let actualData: Observable<any> = itemsService.data$;
    itemsService.get(filters.getValue()).subscribe();
    const req = http.expectOne(expectedReqUrl);
    expect(req.request.method).toEqual('GET');
    req.flush({ data: expectedData });
    actualData.subscribe((data) => {
      expect(data).toEqual(expectedData);
    });
    http.verify();
  });

  it('remove method should return {ok:1}', inject([ItemsDataService], (itemsService: any) => {
    const itemId = 123;
    const reqUrl = `${Api.DATA_ITEMS}/${itemId}`;
    const respBody = { ok: 1 };
    const http = TestBed.inject(HttpTestingController);

    itemsService.remove(itemId).subscribe((response: any) => expect(response.ok).toBeTruthy());

    const req = http.expectOne(reqUrl);
    expect(req.request.method).toBe('DELETE');
    req.flush(respBody);

    http.verify();
  }));
});
