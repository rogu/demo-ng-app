import { HttpClient } from '@angular/common/http';
import { Injectable, Signal, WritableSignal, effect, inject, signal } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Api } from 'src/app/shared/utils/api';
import { HttpArrayStore } from '../../../shared/http-store/http.array.store';
import { FiltersModel, ItemModel } from '../models/items.models';

@Injectable({
  providedIn: 'root',
})
export class ItemsDataService extends HttpArrayStore<ItemModel> {
  authService = inject(AuthService);

  private _filters: WritableSignal<FiltersModel> = signal({
    title: '',
    priceFrom: 0,
    category: '',
    currentPage: 1,
    itemsPerPage: 5,
  });
  get filters(): Signal<FiltersModel> {
    return this._filters.asReadonly();
  }
  constructor(http: HttpClient) {
    super(http, Api.DATA_ITEMS);
    effect(() => this.get(this.filters()).subscribe());
  }

  updateFilters(value: { [key in keyof FiltersModel]: any }) {
    this._filters.update((val) => ({ ...val, ...value }));
  }
}
