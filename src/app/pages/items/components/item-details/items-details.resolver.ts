import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';
import { ItemsDataService } from '../../services/items-data.service';
import { ItemModel, ItemResolver } from '../../models/items.models';

@Injectable({ providedIn: 'root' })
export class ItemDetailsResolver implements ItemResolver<ItemModel> {
  itemsDataService = inject(ItemsDataService);

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.itemsDataService.data$.pipe(
      filter((data: ItemModel[] | null) => !!data?.length),
      map((data: ItemModel[] | null) =>
        data?.find((item: any) => item.id === route.params['id'])
      ),
      first()
    );
  }
}
