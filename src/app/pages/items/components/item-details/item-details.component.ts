import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AsyncPipe } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

@Component({
    selector: 'item-details',
    templateUrl: './item-details.component.html',
    standalone: true,
    imports: [MatCardModule, AsyncPipe]
})
export class ItemDetailsComponent implements OnInit {
    route = inject(ActivatedRoute);
    data$!: Observable<Data>;

    ngOnInit(): void {
        this.data$ = this.route.data.pipe(map(({ item }) => item));
    }

}
