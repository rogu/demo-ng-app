import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreService } from '../../../../core/services/core.service';
import { SearchComponent } from '../../../../shared/components/search/search.component';
import { CamelCaseToSignPipe } from '../../../../shared/pipes/camel-case-to-sign/camel-case-to-sign';
import { ItemsDataService } from '../../services/items-data.service';
import { ItemsComponent } from './items.component';

describe('Items component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [FormsModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        ItemsComponent,
        SearchComponent,
        CamelCaseToSignPipe],
    providers: [
      ItemsDataService,
        CoreService,
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting()
    ]
});
  });

  describe('when filters change', () => {
    it('should call fetchItems() after 500 ms', fakeAsync(() => {
      TestBed
        .overrideComponent(ItemsComponent, {
          set: {
            template: '<div>Overridden template here</div>'
          }
        });
      const fixture = TestBed.createComponent(ItemsComponent);
      const instance = fixture.componentInstance;
      spyOn(instance.dataService, 'get');
      fixture.detectChanges();
      instance.dataService.updateFilters({ itemsPerPage: 20 });
      tick(500);
      expect(instance.dataService.get).toHaveBeenCalled();
    }));
  });

  describe('when input in searchComponent changed', () => {
    it('should call fetchItems()', fakeAsync(async () => {
      TestBed
        .overrideComponent(ItemsComponent, {
          set: {
            template: `
                        <app-search
                            [controls]='searchConfig'
                            (searchChange)='updateFilters($event)'>
                        </app-search>`
          }
        });
      const fixture = TestBed.createComponent(ItemsComponent);
      const instance = fixture.componentInstance;
      spyOn(instance.dataService, 'get');
      fixture.detectChanges();
      tick(500);
      await fixture.whenStable();
      const searchByTitle = fixture.debugElement.query(By.css('#search-by-title')).nativeElement;
      searchByTitle.value = 'cheese';
      searchByTitle.dispatchEvent(new Event('input'));
      tick(500);
      expect(instance.dataService.get).toHaveBeenCalled();
    }));
  });

});
