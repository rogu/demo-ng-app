import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AsyncPipe } from '@angular/common';
import { Component, Signal, TemplateRef, computed, inject, viewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatOptionModule } from '@angular/material/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { Router, RouterOutlet } from '@angular/router';
import { Observable, map } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { Actions } from 'src/app/shared/http-store/http.models';
import { DataGridComponent } from '../../../../shared/components/data-grid/data-grid.component';
import {
  FieldTypes as DGFieldTypes,
  DataGridRowConfig,
  GridAction,
} from '../../../../shared/components/data-grid/data-grid.models';
import { SearchComponent } from '../../../../shared/components/search/search.component';
import { SearchControlModel, FieldTypes as SearchFieldTypes } from '../../../../shared/components/search/search.model';
import { FileUploadComponent } from '../../../../shared/components/upload/file-upload.component';
import { ItemsDataService } from '../../services/items-data.service';
import { FiltersKeys, FiltersModel, ItemKeys, ItemModel } from '../../models/items.models';
import { ModalComponent } from './../../../../shared/components/modal/modal.component';
import { Api } from './../../../../shared/utils/api';

@Component({
  templateUrl: './items.component.html',
  standalone: true,
  imports: [
    MatSidenavModule,
    SearchComponent,
    MatButtonModule,
    ModalComponent,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    FileUploadComponent,
    DataGridComponent,
    MatPaginatorModule,
    RouterOutlet,
    AsyncPipe,
  ],
})
export class ItemsComponent {
  dataService = inject(ItemsDataService);
  authService = inject(AuthService);
  router = inject(Router);
  breakpointObserver = inject(BreakpointObserver);
  dialog = inject(MatDialog);
  items: Observable<ItemModel[] | null> = this.dataService.data$;
  total: Observable<number | null> = this.dataService.total$;
  access: Signal<boolean> = this.authService.access;
  filters: Signal<FiltersModel> = this.dataService.filters;
  uploadUrl: string = Api.DATA_UPLOAD;
  modalRef = viewChild<ModalComponent>('modal');
  moreTplRef = viewChild<TemplateRef<any>>('moreTpl');
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map((result) => result.matches),
    shareReplay()
  );
  dataGridConfig: Signal<DataGridRowConfig<ItemKeys>[]> = computed(() => [
    { key: 'title' },
    { key: 'price', type: DGFieldTypes.INPUT, disabled: !this.access() },
    { key: 'imgSrc', type: DGFieldTypes.IMAGE },
    { key: 'category' },
    { type: DGFieldTypes.BUTTON, disabled: !this.access(), header: 'remove' },
    { type: DGFieldTypes.BUTTON, header: 'more' },
  ]);
  searchConfig: SearchControlModel<FiltersKeys>[] = [
    { name: 'title', tag: SearchFieldTypes.INPUT_TEXT },
    { name: 'priceFrom', tag: SearchFieldTypes.SLIDER },
    {
      name: 'category',
      tag: SearchFieldTypes.SELECT,
      value: '',
      options: ['food', 'clothes'],
    },
  ];

  onAction(action: GridAction): void {
    switch (action.type) {
      case Actions.Add:
        this.dataService.add(action.data).subscribe(() => {
          this.dataService.get(this.filters()).subscribe();
          this.modalRef()!.dialog.closeAll();
        });
        break;
      case Actions.Remove:
        confirm('are you sure?') &&
          this.dataService.remove(action.data.id).subscribe(() => this.dataService.get(this.filters()).subscribe());
        break;
      case Actions.Update:
        this.dataService.update(action.data);
        break;
      case Actions.More:
        this.router.navigate(['pages/items', action.data.id]);
        this.dialog.open(this.moreTplRef()!);
        break;
    }
  }

  updateFilters(filter: any) {
    this.dataService.updateFilters(filter);
  }
}
