import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';

export type ItemKeys = 'title' | 'price' | 'imgSrc' | 'category' | 'id';

export interface ItemModel {
  category: string;
  imgSrc: string;
  price: number;
  title: string;
  id: string;
}

export interface ItemResolver<T> {
  resolve(route: ActivatedRouteSnapshot): Observable<T>
}

export type FiltersKeys = 'title' | 'priceFrom' | 'category' | 'currentPage' | 'itemsPerPage';

export interface FiltersModel {
  title?: string;
  priceFrom?: number;
  category?: string;
  currentPage?: number;
  itemsPerPage?: number;
}

