import { Routes } from '@angular/router';
import { ItemDetailsResolver } from './items/components/item-details/items-details.resolver';
import { MainComponent } from '../core/components/main/main.component';
import { authGuard } from '../core/guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [authGuard],
    component: MainComponent,
    children: [
      {
        path: 'items',
        loadComponent: () => import('./items/components/items/items.component').then((c) => c.ItemsComponent),
        children: [
          {
            path: ':id',
            resolve: { item: ItemDetailsResolver },
            loadComponent: () =>
              import('./items/components/item-details/item-details.component').then((c) => c.ItemDetailsComponent),
          },
        ],
      },
      {
        path: 'workers',
        loadComponent: () => import('./workers/components/workers/workers.component').then((c) => c.WorkersComponent),
      },
      { path: 'home', loadComponent: () => import('./home/home.component').then((c) => c.HomeComponent) },
    ],
  },
];
