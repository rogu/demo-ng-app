import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Api } from 'src/app/shared/utils/api';
import { HttpArrayStore } from '../../../shared/http-store/http.array.store';
import { WorkerModel, WorkersFilters } from '../models/workers.models';

@Injectable({
  providedIn: 'root',
})
export class WorkersDataService extends HttpArrayStore<WorkerModel> {
  private _filters$: BehaviorSubject<WorkersFilters> = new BehaviorSubject({});
  get filters$(): Observable<WorkersFilters> {
    return this._filters$.asObservable();
  }

  constructor(http: HttpClient) {
    super(http, Api.DATA_WORKERS);
    super.get().subscribe();
  }

  updateFilters(value: {}) {
    this._filters$.next({ ...this._filters$.value, ...value });
  }
}
