import { HttpMetodNames } from "src/app/shared/http-store/http.models";

export type WorkerKeys = 'name' | 'phone' | 'category';

export interface WorkerModel {
  id: string,
  name: string,
  phone: number,
  category: string
}

export interface WorkerAction {
  type: HttpMetodNames;
  data: WorkerModel
}

export interface WorkersFilters {
  name?: string,
  phone?: number
}
