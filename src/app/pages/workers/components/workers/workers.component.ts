import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AsyncPipe, KeyValuePipe } from '@angular/common';
import { Component, TemplateRef, inject, viewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatOptionModule } from '@angular/material/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { DataGridComponent } from '../../../../shared/components/data-grid/data-grid.component';
import {
  FieldTypes as DataGridFieldTypes,
  DataGridRowConfig,
  GridAction,
} from '../../../../shared/components/data-grid/data-grid.models';
import { ErrorsComponent } from '../../../../shared/components/errors/errors.component';
import { SearchComponent } from '../../../../shared/components/search/search.component';
import { SearchControlModel, FieldTypes as SearchFieldTypes } from '../../../../shared/components/search/search.model';
import { SearchPipe } from '../../../../shared/pipes/search/search-pipe';
import { WorkersDataService } from '../../services/workers-data.service';
import { WorkerKeys, WorkerModel } from '../../models/workers.models';
import { ModalComponent } from './../../../../shared/components/modal/modal.component';

@Component({
  templateUrl: './workers.component.html',
  standalone: true,
  imports: [
    MatDialogModule,
    MatSidenavModule,
    SearchComponent,
    MatButtonModule,
    MatIconModule,
    ModalComponent,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ErrorsComponent,
    MatSelectModule,
    MatOptionModule,
    DataGridComponent,
    AsyncPipe,
    KeyValuePipe,
    SearchPipe,
  ],
})
export class WorkersComponent {
  dataService = inject(WorkersDataService);
  filters$: Observable<any> = this.dataService.filters$;
  workers$: Observable<WorkerModel[] | null> = this.dataService.data$;
  breakpointObserver = inject(BreakpointObserver);
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map((result) => result.matches),
    shareReplay()
  );
  dialog = inject(MatDialog);
  modalRef = viewChild<ModalComponent>('modal');
  moreTplRef = viewChild<TemplateRef<any>>('more');
  searchConfig: SearchControlModel<WorkerKeys>[] = [
    { name: 'name', tag: SearchFieldTypes.INPUT_TEXT },
    { name: 'phone', tag: SearchFieldTypes.INPUT_TEXT },
    {
      name: 'category',
      tag: SearchFieldTypes.SELECT,
      value: '',
      options: ['sales', 'support'],
    },
  ];
  dataGridConfig: DataGridRowConfig<WorkerKeys>[] = [
    { key: 'name' },
    { key: 'phone', type: DataGridFieldTypes.INPUT },
    { type: DataGridFieldTypes.BUTTON, header: 'remove' },
    { type: DataGridFieldTypes.BUTTON, header: 'more' },
  ];

  actionHandler({ type, data }: GridAction) {
    switch (type) {
      case 'add':
        this.dataService.add(data).subscribe(() => {
          this.dataService.get().subscribe();
          this.modalRef()!.dialog.closeAll();
        });
        break;
      case 'remove':
        if (confirm('are you sure?')) this.dataService.remove(data.id).subscribe((resp) => this.dataService.get().subscribe());
        break;
      case 'update':
        this.dataService.update(data).subscribe();
        break;
      case 'more':
        this.dialog.open(this.moreTplRef()!, { data });
        break;
    }
  }

  updateFilters(value: {}) {
    this.dataService.updateFilters(value);
  }
}
