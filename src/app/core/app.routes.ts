import { Routes } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { checkTokenGuard } from './guards/check-token.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [checkTokenGuard],
    children: [
      { path: 'auth/:tab', component: AuthComponent },
      {
        path: 'pages',
        loadChildren: () => import('../pages/pages.routes').then((m) => m.routes),
      },
      { path: '**', redirectTo: '/pages/home' },
    ],
  },
];
