export interface AuthServiceModel {
  isLogged(): void;
  logIn(value: { username: string, password: string }): void;
  logOut(): void;
}

export interface AuthDataModel {
  id:string;
  username: string;
  password: string;
}

export interface AuthResDataModel {
  id:string;
  username: string;
  accessToken: string;
}