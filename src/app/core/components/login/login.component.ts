import { Component, inject } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, MatButtonModule, FormsModule],
  template: `
    <div class="p-4 lg:w-1/2">
      <h2 class="text-blue-900">
        Aby kontynuować, zaloguj się.
        <br />
        Username i hasło są wpisane.
      </h2>
      <form id="login-form" (submit)="logIn(loginForm.value)" #loginForm="ngForm" class="flex flex-col space-y-2">
        <mat-form-field>
          <mat-label>username</mat-label>
          <input matInput type="email" name="username" ngModel="admin@localhost" required placeholder="username" />
        </mat-form-field>
        <mat-form-field>
          <mat-label>password</mat-label>
          <input matInput type="password" name="password" ngModel="Admin1" required placeholder="password" />
        </mat-form-field>
        <div>
          <button mat-raised-button color="primary">log in</button>
        </div>
      </form>
    </div>
  `,
})
export class LoginComponent {
  authService = inject(AuthService);
  logIn(formValue: any) {
    this.authService.logIn(formValue);
  }
}
