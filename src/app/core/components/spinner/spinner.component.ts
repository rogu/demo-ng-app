import { AsyncPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouteConfigLoadStart, Router } from '@angular/router';
import { Observable, iif, merge, of } from 'rxjs';
import { delay, map, switchMap } from 'rxjs/operators';
import { CoreService } from '../../services/core.service';

@Component({
  selector: 'app-spinner',
  template: `
    @if (show$|async) {
    <div class="wrapper">
      <div class="circle">
        <mat-spinner diameter="40" color="primary"></mat-spinner>
      </div>
    </div>
    }
  `,
  styles: [
    `
      .wrapper {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: rgba(255, 255, 255, 0.6);
        left: 0;
        z-index: 100;
        top: 0;
        transition: background-color 200ms linear;
        display: flex;
      }
      .circle {
        margin: auto;
      }
    `,
  ],
  standalone: true,
  imports: [MatProgressSpinnerModule, AsyncPipe],
})
export class SpinnerComponent {
  router = inject(Router);
  coreService = inject(CoreService);
  loading$: Observable<boolean> = this.coreService.httpActive$;
  show$ = merge(
    this.loading$, 
    this.router.events.pipe(map((ev) => ev instanceof RouteConfigLoadStart)))
    .pipe(
      switchMap((ev: boolean) => 
        iif(() => ev, of(true), of(false).pipe(delay(500))))
  );
}
