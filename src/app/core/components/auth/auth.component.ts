import { AsyncPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['auth.component.scss'],
  standalone: true,
  imports: [
    MatTabsModule,
    RegisterComponent,
    AsyncPipe,
    LoginComponent,
    MatButtonModule,
    RouterModule
  ],
})
export class AuthComponent {
  authService = inject(AuthService);
  route = inject(ActivatedRoute);
  access = this.authService.access;

  setTab(idx: any) {
    window.history.pushState('', '', `auth/${idx === 0 ? 'login' : 'register'}`);
  }

  logOut() {
    this.authService.logOut();
  }
}
