declare var require: any;
const { version } = require('../../../../../package.json');
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AsyncPipe } from '@angular/common';
import { Component, inject, Signal, viewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map, shareReplay } from 'rxjs/operators';
import { IMenu, MenuComponent } from 'src/app/core/components/menu/menu.component';
import { AuthService } from '../../services/auth.service';
import { SpinnerComponent } from '../spinner/spinner.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  standalone: true,
  imports: [
    MatSidenavModule,
    MatToolbarModule,
    RouterLink,
    MatListModule,
    RouterLinkActive,
    MatButtonModule,
    MatIconModule,
    RouterOutlet,
    SpinnerComponent,
    AsyncPipe,
    MenuComponent,
  ],
})
export class MainComponent {
  version = version;
  breakpointObserver = inject(BreakpointObserver);
  authService = inject(AuthService);
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map((result) => result.matches),
    shareReplay()
  );
  username$: Observable<any> = this.authService.data$.pipe(map((data) => data?.username));
  access: Signal<any> = this.authService.access;
  drawer = viewChild<any>('drawer');
  menus: IMenu[] = [
    { path: '/pages/home', name: 'home' },
    { path: '/pages/items', name: 'items' },
    { path: '/pages/workers', name: 'workers' },
  ];

  navClickHandler() {
    this.isHandset$.pipe(first()).subscribe((mobile) => {
      mobile && this.drawer()!.toggle();
    });
  }

  logOut() {
    this.authService.logOut();
  }
}
