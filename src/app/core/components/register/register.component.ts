import { JsonPipe, KeyValuePipe, NgClass } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  FormGroupDirective,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Observable } from 'rxjs';
import { ErrorsComponent } from 'src/app/shared/components/errors/errors.component';
import { CustomServerValidatorsService } from 'src/app/shared/validators/custom-server-validators.service';
import { CustomValidators } from 'src/app/shared/validators/custom-validators';
import { NotificationService } from '../../services/notification.service';
import { RegisterService } from '../../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    ErrorsComponent,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatButtonModule,
    MatCardModule,
    NgClass,
    JsonPipe,
    KeyValuePipe,
  ],
})
export class RegisterComponent {
  registerService = inject(RegisterService);
  notification = inject(NotificationService);
  customServerValidators = inject(CustomServerValidatorsService);
  registerForm: FormGroup = new FormGroup({
    username: new FormControl(
      '',
      [Validators.required, Validators.email],
      (control: any): Observable<any> => this.customServerValidators.checkUsername({ username: control.value })
    ),
    passwordGroup: new FormGroup({
      password: new FormControl(null, [Validators.required, CustomValidators.passwordRegEx]),
      confirmPassword: new FormControl('', [Validators.required, CustomValidators.equalRequired]),
    }),
    birthDate: new FormControl(null, [Validators.required, CustomValidators.dateShouldBeLessThan('1/1/2022')]),
    hobbies: new FormGroup(
      {
        tv: new FormControl(),
        music: new FormControl(),
        sport: new FormControl(),
        travel: new FormControl(),
      },
      CustomValidators.atLeastOneShouldBeSelected
    ),
    specialRequests: new FormArray([new FormControl()]),
  });

  addHobby(name: string) {
    name && (this.registerForm.controls.hobbies as FormGroup).addControl(name, new FormControl(true));
  }

  addSpecialRequest() {
    (this.registerForm.controls.specialRequests as FormArray).push(new FormControl());
  }

  orderPassword(x: any) {
    return x;
  }

  sendForm(ngForm: FormGroupDirective) {
    if (this.registerForm.valid) {
      this.registerService
        .add({
          ...this.registerForm.value,
          password: this.registerForm.value.passwordGroup.password,
          birthDate: Date.parse(Object.values(this.registerForm.value.birthDate).join('-')).toString(),
        })
        .subscribe((_) => {
          ngForm.resetForm();
          this.registerForm.reset();
        });
    } else {
      this.notification.showWarning('form invalid!');
    }
  }
}
