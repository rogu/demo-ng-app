import { Component, Input } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { RouterLink, RouterLinkActive } from '@angular/router';

export interface IMenu {
  path: string;
  name: string;
}

@Component({
  selector: 'app-menu',
  template: `
    @for (menu of _menus; track menu) {
    <a
      mat-list-item
      class="rounded px-4 py-3 bg-blue-400 "
      routerLinkActive="bg-blue-600"
      routerLinkActive="active"
      [routerLink]="menu.path"
      >{{ menu.name }}</a
    >
    }
  `,
  styles: [
    `
      a.active {
        @apply bg-gray-100 border-y border-gray-200;
      }
    `,
  ],
  standalone: true,
  imports: [RouterLink, RouterLinkActive, MatListModule],
})
export class MenuComponent {
  _menus!: IMenu[];
  @Input()
  set menus(menu: IMenu[]) {
    this._menus = menu;
  }
}
