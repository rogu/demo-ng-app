import { Injectable, inject } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

export type MessageTypes = 'info' | 'error' | 'success' | 'warning';

@Injectable({ providedIn: 'root' })
export class NotificationService {

    snackBar = inject(MatSnackBar);

    showInfo(message: string) {
        this.show(message, 'info');
    }

    showError(message: string) {
        this.show(message, 'error');
    }

    showWarning(message: string) {
        this.show(message, 'warning');
    }

    showSuccess(message: string) {
        this.show(message, 'success');
    }

    private show(message: string = 'unknown error', type: MessageTypes) {
        const config = new MatSnackBarConfig();
        config.panelClass = ['notification', type];
        config.duration = 7000;
        config.horizontalPosition = 'left';
        config.verticalPosition = 'bottom';
        this.snackBar.open(message, 'close', config);
    }
}
