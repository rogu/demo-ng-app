import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpArrayStore } from 'src/app/shared/http-store/http.array.store';
import { Api } from '../../shared/utils/api';

@Injectable({ providedIn: 'root' })
export class RegisterService extends HttpArrayStore<any> {
  constructor(http: HttpClient) {
    super(http, Api.AUTH_REGISTER);
  }
}
