import { HttpClient } from '@angular/common/http';
import { Injectable, Signal, WritableSignal, inject, signal } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpObjectStore } from 'src/app/shared/http-store/http.object.store';
import { ResponseData } from 'src/app/shared/http-store/http.models';
import { NotificationService } from './notification.service';
import { Api } from '../../shared/utils/api';
import { AuthDataModel, AuthResDataModel } from '../models/auth.models';

@Injectable({ providedIn: 'root' })
export class AuthService extends HttpObjectStore<AuthResDataModel> {
  notification = inject(NotificationService);
  router = inject(Router);
  private _access: WritableSignal<boolean> = signal<boolean>(false);
  public get access(): Signal<boolean> {
    return this._access.asReadonly();
  }
  private username$: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(http: HttpClient) {
    super(http, Api.AUTH_LOGIN);
  }

  logIn(value: AuthDataModel): void {
    this.post(value, Api.AUTH_LOGIN).subscribe(({ data }: ResponseData<AuthResDataModel>) => {
      this.setToken = data.accessToken;
      this.setUserData(data);
      this.router.navigateByUrl('/pages/items');
    });
  }

  set setToken(token: string) {
    localStorage.setItem('token', token);
  }

  get getToken(): string {
    return localStorage.getItem('token') || '';
  }

  logOut() {
    this.clear();
    this.router.navigateByUrl('/auth/login');
    this.notification.showSuccess('you are logged out');
  }

  refreshToken(): Observable<ResponseData<any>> {
    return this.fetch(Api.AUTH_REFRESH_TOKEN);
  }

  setUserData(data: AuthResDataModel) {
    this._access.set(!!data.username);
    this.username$.next(data.username);
  }

  clear() {
    localStorage.removeItem('token');
    this._access.set(false);
    this.username$.next(null);
  }
}
