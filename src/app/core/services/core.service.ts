import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CoreService {
  httpActive$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
}
