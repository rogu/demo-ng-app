import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { map, tap } from 'rxjs';
import { Api } from 'src/app/shared/utils/api';
import { AuthService } from '../services/auth.service';

export const checkTokenGuard: CanActivateFn = (route, state, authService = inject(AuthService)) => {
  return authService.fetch(Api.AUTH_IS_LOGGED).pipe(
    tap(({ data, warning }) => (warning ? authService.clear() : authService.setUserData(data))),
    map(() => true)
  );
};
