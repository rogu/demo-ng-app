import { HttpErrorResponse, HttpEvent, HttpEventType, HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { asapScheduler, catchError, tap, throwError } from 'rxjs';
import { CoreService } from '../services/core.service';

export const coreInterceptor: HttpInterceptorFn = (req, next) => {
  const coreService = inject(CoreService);
  asapScheduler.schedule(() => coreService.httpActive$.next(true));
  return next(req).pipe(
    tap((evt: HttpEvent<any>) => {
      if (evt.type === HttpEventType.Response) {
        coreService.httpActive$.next(false);
      }
    }),
    catchError(({ error }: HttpErrorResponse) => {
      coreService.httpActive$.next(false);
      return throwError(() => error);
    })
  );
};
