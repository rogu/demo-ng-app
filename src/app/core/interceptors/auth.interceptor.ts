import {
  HttpErrorResponse,
  HttpHeaders,
  HttpInterceptorFn,
  HttpRequest
} from '@angular/common/http';
import { inject } from '@angular/core';
import { catchError, iif, switchMap, tap, throwError } from 'rxjs';
import { ResponseData } from 'src/app/shared/http-store/http.models';
import { AuthResDataModel } from '../models/auth.models';
import { AuthService } from '../services/auth.service';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const authService = inject(AuthService);
  const headers = new HttpHeaders({ authorization: authService.getToken });
  const reqClone = req.clone({ headers });
  return next(reqClone).pipe(
    catchError(({ status, error }: HttpErrorResponse) => {
      switch (status) {
        case 401:
          return refreshToken(req);
      }
      return throwError(() => error);
    })
  );
  function refreshToken(req: HttpRequest<any>) {
    return authService.refreshToken().pipe(
      switchMap((resp: ResponseData<AuthResDataModel>) =>
        iif(
          () => !!resp.data.accessToken,
          next(req.clone({ headers: new HttpHeaders({ authorization: resp.data.accessToken }) })).pipe(
            tap(() => {
              authService.setToken = resp.data.accessToken;
              authService.setUserData(resp.data);
            })
          ),
          throwError(() => resp.error).pipe(tap(() => authService.setUserData(resp.data)))
        )
      ),
      catchError((error) => {
        authService.logOut();
        return throwError(() => error);
      })
    );
  }
};
