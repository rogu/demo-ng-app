import { HttpErrorResponse, HttpEvent, HttpEventType, HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { catchError, tap, throwError } from 'rxjs';
import { NotificationService } from '../services/notification.service';

export const notificationInterceptor: HttpInterceptorFn = (req, next) => {
  const notification = inject(NotificationService);
  return next(req).pipe(
    tap((evt: HttpEvent<any>) => {
      if (evt.type === HttpEventType.Response) {
        showNotification(evt.body);
      }
    }),
    catchError(({ error }: HttpErrorResponse) => {
      showNotification(error);
      return throwError(() => error);
    })
  );

  function showNotification(body: any) {
    switch (true) {
      case !!body.success:
        notification.showSuccess(body.success);
        break;
      case !!body.info:
        notification.showInfo(body.info);
        break;
      case !!body.warning:
        notification.showWarning(body.warning);
        break;
      case !!body.error:
        notification.showError(body.error);
        break;
    }
  }
};
