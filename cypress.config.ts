import { defineConfig } from 'cypress';
const fs = require('fs');
export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:5555',
    video: false,
    excludeSpecPattern: process.env.CY_MODE ? 'cypress/e2e/all.cy.ts' : [],
    setupNodeEvents(on, config) {
      on('task', {
        readFileMaybe(filename) {
          if (fs.existsSync(filename)) {
            return fs.readFileSync(filename, 'utf8');
          }
          return null;
        },
        log(message) {
          console.log(message)
          return null
        }
      })
    }
  },
})
